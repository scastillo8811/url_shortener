### What is this repository for? ###

* URL Shortener Symfony 5 (REST API,Crawler,Events)
* Vuejs (Router, Vuex)

### What how I resolve the problem? ###

* Two Controller 1) REST API (UrlController), 2) Application (Vuejs and redirect and events)
* Two Services 1) CrawlerService (get metadata from the url to short), 2)UrlShortener (Generate the short URL)
* Two Events 1) UrlRedirectSubscriber (Register the visits when a someone visit the short link)  2)BeforeActionSubscriber (Transform the json request in the API REST)
* UnitTesting to vaidate the REST API endpoints
* Frontend Vuejs(Router, Vuex, Vee-validate)


### How do I get set up? ###

* composer install
* setup your .env
* symfony console doctrine:migrations:migrate
* yarn install
* yarn dev
* symfony console serve
