import axios from "axios";

export default {
  create(url_redirect) {
    return axios.post("/api/urls", {
      urlRedirect: url_redirect
    });
  },
  findAll() {
    return axios.get("/api/urls");
  },
  find(url_shorted) {
    return axios.get("/api/urls/"+url_shorted,
    );
  }
};
