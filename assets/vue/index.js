import Vue from "vue";
import App from "./App";
import router from "./router";
import store from "./store";
import {VueMasonryPlugin} from 'vue-masonry';
import VeeValidate from 'vee-validate';


Vue.use(VueMasonryPlugin)
Vue.use(VeeValidate);

new Vue({
  components: { App },
  template: "<App/>",
  router,
  store
}).$mount("#app");
