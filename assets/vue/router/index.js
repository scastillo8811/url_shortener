import Vue from "vue";
import VueRouter from "vue-router";
import Urls from "../views/Urls";
import UrlDetail from "../views/UrlDetail";

import {VueMasonryPlugin} from 'vue-masonry';



Vue.use(VueRouter);
Vue.use(VueMasonryPlugin)


export default new VueRouter({
  mode: "history",
  routes: [
    { path: "/", component: Urls },
    { path: "/urls", component: Urls },
    {
      path: '/view/:urlShorted',
      name: 'url-item',
      component: UrlDetail
    },
    { path: "*", redirect: "/" }
  ]
});
