import UrlAPI from "../api/url";

const CREATING_URL = "CREATING_URL",
  CREATING_URL_SUCCESS = "CREATING_URL_SUCCESS",
  CREATING_URL_ERROR = "CREATING_URL_ERROR",
  FETCHING_URLS = "FETCHING_URLS",
  FETCHING_URLS_SUCCESS = "FETCHING_URLS_SUCCESS",
  FETCHING_URLS_ERROR = "FETCHING_URLS_ERROR",
  FETCHING_URL = "FETCHING_URL",
  FETCHING_URL_SUCCESS = "FETCHING_URL_SUCCESS",
  FETCHING_URL_ERROR = "FETCHING_URL_ERROR";

export default {
  namespaced: true,
  state: {
    isLoading: false,
    error: null,
    urls: []
  },
  getters: {
    isLoading (state) {
      return state.isLoading;
    },
    hasError (state) {
      return state.error !== null;
    },
    error (state) {
      return state.error;
    },
    hasUrls (state) {
      return state.urls.length > 0;
    },
    urls (state) {
      return state.urls;
    }
  },
  mutations: {
    [CREATING_URL] (state) {
      state.isLoading = true;
      state.error = null;
    },
    [CREATING_URL_SUCCESS] (state, url) {
      state.isLoading = false;
      state.error = null;
      state.urls.unshift(url);
    },
    [CREATING_URL_ERROR] (state, error) {
      state.isLoading = false;
      state.error = error;
      state.urls = [];
    },
    [FETCHING_URLS] (state) {
      state.isLoading = true;
      state.error = null;
      state.urls = [];
    },
    [FETCHING_URLS_SUCCESS] (state, urls) {
      state.isLoading = false;
      state.error = null;
      state.urls = urls;
    },
    [FETCHING_URLS_ERROR] (state, error) {
      state.isLoading = false;
      state.error = error;
      state.urls = [];
    },
    [FETCHING_URL] (state) {
      state.isLoading = true;
      state.error = null;
      state.urls = [];
    },
    [FETCHING_URL_SUCCESS] (state, urls) {
      state.isLoading = false;
      state.error = null;
      state.urls = urls;
    },
    [FETCHING_URL_ERROR] (state, error) {
      state.isLoading = false;
      state.error = error;
      state.urls = [];
    }
  },
  actions: {
    async create ({commit}, message) {
      commit(CREATING_URL);
      try {
        let response = await UrlAPI.create(message);
        commit(CREATING_URL_SUCCESS, response.data.data);
        return response.data.data;
      } catch (error) {
        commit(CREATING_URL_ERROR, error);
        return null;
      }
    },
    async findAll ({commit}) {
      commit(FETCHING_URLS);
      try {
        let response = await UrlAPI.findAll();
        commit(FETCHING_URLS_SUCCESS, response.data.data);
        return response.data.data;
      } catch (error) {
        commit(FETCHING_URLS_ERROR, error);
        return null;
      }
    },
    async find ({commit},item) {
      commit(FETCHING_URL);
      try {
        let response = await UrlAPI.find(item);
        commit(FETCHING_URL_SUCCESS, response.data.data);
        return response.data.data;
      } catch (error) {
        commit(FETCHING_URL_ERROR, error);
        return null;
      }
    }
  }
}
