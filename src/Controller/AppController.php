<?php

namespace App\Controller;

use App\Entity\Url;
use App\EventSubscriber\UrlRedirectSubscriber;
use App\event\LinkRedirectEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;



class AppController extends AbstractController
{

    /**
     * @Route("/{vueRouting}", name="home")
     * @Route("/", name="home_sf")
     * @Route("/view/{vueRouting}", name="item")

     */
    public function vueRouting()
    {
        return $this->render('base.html.twig', []);
    }

    /**
     * @Route("/go/{urlShorted}", name="url_redirect", methods={"GET"})
     */
    public function show(Url $url, EntityManagerInterface $em)
    {
        $event = new LinkRedirectEvent($url);
        $dispatcher = new EventDispatcher();
        $subscriber = new UrlRedirectSubscriber($em);

        $dispatcher->addSubscriber($subscriber);
        $dispatcher->dispatch($event,LinkRedirectEvent::NAME);

        return $this->redirect($url->getUrlRedirect());

    }
}
