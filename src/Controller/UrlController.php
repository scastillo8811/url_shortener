<?php

namespace App\Controller;

use App\Entity\Url;
use App\Form\UrlType;
use App\Repository\UrlRepository;
use App\Service\UrlMetadaCrawler;
use App\Service\UrlShortener;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;


/**
 * @Route("/api")
 */
class UrlController extends AbstractController
{
    protected $urlRepository;

    public function __construct(UrlRepository $urlRepository, SerializerInterface $serializer)
    {
        $this->urlRepository = $urlRepository;
        $this->serializer = $serializer;
    }


    /**
     * @Route("/urls", name="url_index", methods={"GET"})
     */
    public function fetch(Request $request): Response
    {
        $urls = $this->urlRepository->findAll(['id' => 'DESC']);

        return new JsonResponse(
            ['message' => 'OK', 'data' => $urls],
            Response::HTTP_OK);
    }

    /**
     * @Route("/urls", name="url_create", methods={"POST"})
     */
    public function new(Request $request, ValidatorInterface $validator, UrlShortener $urlShortener, UrlMetadaCrawler $metadaCrawler)
    {
        $errors_data = [];

        $url = new Url();
        $url->setDescription($request->get('description', null));
        $url->setUrlRedirect($request->get('urlRedirect'));
        $url->setUrlShorted($request->get('urlShorted'));

        $metas = $metadaCrawler->getMetaData($url->getUrlRedirect());

        $url->setMetaDescription($metas['metadescription']);
        $url->setPageTitle($metas['pagetitle']);
        $url->setMetaTags(json_encode($metas['metatags']));
        $url->setCanonicalUrl($metas['canonical_url']);
        $url->setImage($metas['image']);

        if (!$url->getDescription()) {
            $url->setDescription($url->getMetaDescription());
        }

        $new_url = $urlShortener->generate();
        $url->setUrlShorted($new_url);

        $errors = $validator->validate($url);

        if (count($errors) > 0) {

            foreach ($errors as $error) {
                $errors_data[] = ['field' => $error->getPropertyPath(), 'message' => $error->getMessage()];
            }

            return new JsonResponse(
                [
                    'message' => 'URL Not created!',
                    'errors' => $errors_data

                ],
                Response::HTTP_BAD_REQUEST);
        } else {
            $this->urlRepository->saveFromEntity($url);

            return new JsonResponse(
                ['message' => 'URL created!', 'data' => $url],
                Response::HTTP_CREATED);
        }

        return new JsonResponse(
            ['message' => 'URL Not created!'],
            Response::HTTP_CREATED);
    }

    /**
     * @Route("/urls/{urlShorted}", name="url_show", methods={"GET"})
     */
    public function show(Url $url): Response
    {
        $url->getVisits();
        return new JsonResponse(
            ['message' => 'OK', 'data' => $url],
            Response::HTTP_OK);
    }

    /**
     * @Route("/urls/{urlShorted}", name="url_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Url $url): Response
    {
        $this->urlRepository->remove($url);
        $this->urlRepository->flush();

        return new JsonResponse(
            ['message' => 'URL Deleted', 'data' => $url],
            Response::HTTP_OK);
    }
}
