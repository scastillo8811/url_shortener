<?php

namespace App\Entity;

use App\Repository\UrlRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UrlRepository::class)
 */
class Url implements \JsonSerializable
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank
     */
    private $urlShorted;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\Url
     */
    private $urlRedirect;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="json", length=255, nullable=true)
     * @Assert\Json
     */
    private $metaTags;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $canonicalUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pageTitle;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Visit", mappedBy="url", fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"created" = "DESC"})
     */
    private $visits;

    public function __construct()
    {
        $this->visits= new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getUrlShorted(): ?string
    {
        return $this->urlShorted;
    }

    public function setUrlShorted($urlShorted): self
    {
        $this->urlShorted = $urlShorted;

        return $this;
    }

    public function getUrlRedirect(): ?string
    {
        return $this->urlRedirect;
    }

    public function setUrlRedirect($urlRedirect): self
    {
        $this->urlRedirect = $urlRedirect;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description ? $this->description : $this->metaDescription;
    }

    public function setDescription($description): self
    {
        $this->Description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        if(!$this->image){
            $this->image='http://www.gravatar.com/avatar/'.$this->getUrlShorted().'?d=identicon&size=256';
        }

        return $this->image;
    }

    public function setImage($image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getMetaTags()
    {
        return json_decode($this->metaTags, true);
    }

    public function setMetaTags($metaTags): self
    {
        $this->metaTags = $metaTags;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription($metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getCanonicalUrl(): ?string
    {
        return $this->canonicalUrl;
    }

    public function setCanonicalUrl($canonicalUrl): self
    {
        $this->canonicalUrl = $canonicalUrl;

        return $this;
    }

    public function getPageTitle(): ?string
    {
        return $this->pageTitle;
    }

    public function setPageTitle($pageTitle): self
    {
        $this->pageTitle = $pageTitle;

        return $this;
    }

    /**
     * @return Collection|Visit[]
     */
    public function getVisits()
    {
        return $this->visits;
    }

    public function addVisit(Visit $visit): self
    {
        if (!$this->visits->contains($visit)) {
            $this->visits[] = $visit;
        }

        return $this;
    }

    public function removeVisit(Visit $visit): self
    {
        if ($this->visits->contains($visit)) {
            $this->visits->removeElement($visit);
        }

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'description' => $this->getDescription(),
            'url_shortened' => $this->getUrlShorted(),
            'url_redirect' => $this->getUrlRedirect(),
            'pagetitle' => $this->getPageTitle(),
            'metadescription' => $this->getMetaDescription(),
            'metatags' => $this->getMetaTags(),
            'image' => $this->getImage(),
            'canonical_url' => $this->getCanonicalUrl(),
            'visits'=>$this->getVisits()->toArray()
        ];
    }
}
