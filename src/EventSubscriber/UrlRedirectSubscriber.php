<?php


namespace App\EventSubscriber;


use App\Entity\Visit;
use App\event\LinkRedirectEvent;
use App\Repository\UrlRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UrlRedirectSubscriber implements EventSubscriberInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {
        return [
            LinkRedirectEvent::NAME => 'onUrlRedirect',
        ];
    }

    public function onUrlRedirect($event)
    {
        $url = $event->getUrl();
        $visit = new Visit();

        $now = new \DateTime();
        $visit->setCreated($now);
        $visit->setDevice($_SERVER['HTTP_USER_AGENT']);
        $visit->setIp($_SERVER['REMOTE_ADDR']);
        $visit->setUrl($url);
        $this->em->persist($visit);
        $this->em->flush();
    }

}
