<?php

namespace App\Repository;

use App\Entity\Url;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Url|null find($id, $lockMode = null, $lockVersion = null)
 * @method Url|null findOneBy(array $criteria, array $orderBy = null)
 * @method Url[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UrlRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Url::class);
        $this->manager=$manager;
    }

    public function findAll($orderBy=[])
    {
        return $this->findBy([],$orderBy);
    }

    public function save($name, $description, $urlShorted, $urlRedirect,$pageTitle,$metaTags,$metaDescription)
    {
        $url = new Url();

        $url
            ->setName($name)
            ->setDescription($description)
            ->setUrlShorted($urlShorted)
            ->setUrlRedirect($urlRedirect)
            ->setPageTitle($pageTitle)
            ->setMetaTags($metaTags)
            ->setMetaDescription($metaDescription);


        $this->persist($url);
        $this->flush();
    }

    public function saveFromEntity(Url $url){
        $this->manager->persist($url);
        $this->manager->flush();
    }
}
