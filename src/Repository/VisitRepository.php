<?php

namespace App\Repository;

use App\Entity\Url;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Url|null find($id, $lockMode = null, $lockVersion = null)
 * @method Url|null findOneBy(array $criteria, array $orderBy = null)
 * @method Url[]    findAll()
 * @method Url[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VisitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Url::class);
        $this->manager=$manager;
    }

    // /**
    //  * @return Url[] Returns an array of Url objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Url
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function save($name, $description, $urlShorted, $urlRedirect,$pageTitle,$metaTags,$metaDescription)
    {
        $url = new Url();

        $url
            ->setName($name)
            ->setDescription($description)
            ->setUrlShorted($urlShorted)
            ->setUrlRedirect($urlRedirect)
            ->setPageTitle($pageTitle)
            ->setMetaTags($metaTags)
            ->setMetaDescription($metaDescription);


        $this->persist($url);
        $this->flush();
    }

    public function saveFromEntity(Url $url){
        $this->manager->persist($url);
        $this->manager->flush();
    }
}
