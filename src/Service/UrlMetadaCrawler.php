<?php


namespace App\Service;


use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class UrlMetadaCrawler
{
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getMetaData($url)
    {
        $data = [
            'metadescription' => null,
            'pagetitle' => null,
            'metatags' => null,
            'image' => null,
            'canonical_url' => null
        ];

        $content = $this->fetchContent($url);

        if ($content) {
            $crawler = new Crawler();
            $crawler->addHtmlContent($content, 'UTF-8');

            $data['metadescription'] = $crawler->filterXpath("//meta[@name='description']")->extract(['content']);

            if (!empty($data['metadescription'])) {
                $data['metadescription'] = $data['metadescription'][0];
            } else {
                $data['metadescription'] = null;
            }

            $data['metatags'] = $crawler->filterXpath("//meta[@name='keywords']")->extract(['content']);
            if (!empty($data['metatags'])) {
                $data['metatags'] = explode(', ', $data['metatags'][0]);
            } else {
                $data['metatags'] = null;
            }

            $data['image'] = $crawler->filterXpath("//meta[@property='og:image']")->extract(['content']);
            if (!empty($data['image'])) {
                $data['image'] = $data['image'][0];
            } else {
                $data['image'] = null;
            }

            $data['pagetitle'] = $crawler->filterXpath("//title")->text();

            $data['canonical_url'] = $crawler->filterXpath("//link[@rel='canonical']")->extract(['href']);
            if (!empty($data['canonical_url'])) {
                $data['canonical_url'] = $data['canonical_url'][0];
            } else {
                $data['canonical_url'] = null;
            }
        }

        return $data;

    }

    public function fetchContent($url)
    {
        try {
            $response = $this->client->request(
                'GET',
                $url
            );

            $statusCode = $response->getStatusCode();

            if ($statusCode == Response::HTTP_OK) {
                $content = $response->getContent(false);
                return $content;
            }

        } catch (TransportException $e) {
            return false;
        }

    }
}
