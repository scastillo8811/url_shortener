<?php
namespace App\event;


use App\Entity\Url;
use Symfony\Contracts\EventDispatcher\Event;

class LinkRedirectEvent extends Event
{
    public const NAME = 'url.redirected';

    protected $url;

    public function __construct(Url $url)
    {
        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }

}
