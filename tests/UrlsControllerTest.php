<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;

class UrlsControllerTest extends WebTestCase
{

    public function testGetUrls()
    {
        $client = static::createClient();
        $client->request('GET', '/api/urls');
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPostUrl()
    {
        $data = [
            'urlRedirect' => 'http://google.com'
        ];

        $client = static::createClient();
        $client->request('POST',
            '/api/urls',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );
        $response = $client->getResponse();


        $this->assertEquals(201, $response->getStatusCode());

    }

    public function testWrongPostUrl()
    {
        $data = [
            'urlRedirect' => '//google.xx'
        ];

        $client = static::createClient();
        $client->request('POST',
            '/api/urls',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );
        $response = $client->getResponse();

        $this->assertNotEquals(201, $response->getStatusCode());

    }
}
